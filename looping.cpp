#include <iostream>
#include <vector>

using namespace std;

void printMe(int n, int i, vector<int> v) {
  if (n == i) {
    return;
  }
  cout << v.at(i%10) << endl;
  printMe(n, i+1, v);
}

int main() {
  int n;
  cin >> n;
  cout << endl;

  cout << "int i loop: " << endl;
  for (int i = 0; i < n; i++) {
    cout << (i%10) << endl;
  }

  cout << endl << "While loop: " << endl;
  int modN = 0;
  while (modN < n) {
    cout << (modN%10) << endl;
    modN++;
  }

  cout << endl << "iterator loop: " << endl;
  vector<int> v = {0, 1, 2, 3 ,4 ,5 ,6 ,7 ,8 ,9};
  modN = n;
  while (modN > 0) {
    for (auto i = v.begin(); i != v.end(); i++) {
      if (modN-- == 0) {
        break;
      }
      cout << *i << endl;
    }
  }

  cout << endl << "range loop: " << endl;
  modN = n;
  while (modN > 0) {
    for (auto i : v) {
      if (modN-- == 0) {
        break;
      }
      cout << i << endl;
    }
  }

  cout << endl << "recursion loop: " << endl;
  printMe(n, 0, v);

  return 0;
}
